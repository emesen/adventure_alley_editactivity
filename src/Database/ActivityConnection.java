/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import Bricks.Activity;

import Bricks.Restriction;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pelle
 */
public class ActivityConnection {

    private Connection connection = null;
    
    public void saveActivity(Activity activity) {
    
        try {
            
            String sql = "INSERT INTO days (monday, tuesday, wedensday, thursday, friday, saturday , sunday)"
                    + " VALUES(?,?,?,?,?,?,?)";
            
            connection = DBConnection.getConnection();
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                
                preparedStatement.setInt(1, activity.getDays()[0]);
                preparedStatement.setInt(2, activity.getDays()[1]);
                preparedStatement.setInt(3, activity.getDays()[2]);
                preparedStatement.setInt(4, activity.getDays()[3]);
                preparedStatement.setInt(5, activity.getDays()[4]);
                preparedStatement.setInt(6, activity.getDays()[5]);
                preparedStatement.setInt(7, activity.getDays()[6]);
                System.out.println(activity.getDays()[0]);
                preparedStatement.executeUpdate();
                
            } catch (SQLException ex) {
                Logger.getLogger(ActivityConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
            int maxActivityID = getMaxActivityID(connection);
            
            int chosenDays = getdays(connection);
            String SaveRestriction = "INSERT into restriction (name, restrictionValue) VALUES (?,?)";
            
            
            
            PreparedStatement preparedStatement = connection.prepareStatement(SaveRestriction);
            for(int i = 0; i < activity.getRestrictionList().size(); i++) {
                preparedStatement.setString(1, activity.getRestrictionList().get(i).getName());
                preparedStatement.setInt(2, activity.getRestrictionList().get(i).getRestriction());
                
                preparedStatement.executeUpdate();
            }
            String saveActivity = "INSERT INTO activity (name,  instructor,"
                    + "daysID_fk, start_time, end_time) VALUES (?,?,?,?,?)";
            
            try {
                PreparedStatement preparedStatement2 = connection.prepareStatement(saveActivity);
                
                preparedStatement2.setString(1, activity.getName());
                
                preparedStatement2.setString(2, activity.getInstructor());
                
                
                preparedStatement2.setInt(3, chosenDays);
                preparedStatement2.setString(4, activity.getStartTime());
                preparedStatement2.setString(5, activity.getEndTime());
                
                preparedStatement.executeUpdate();
                for(int i = 0; i < activity.getRestrictionList().size(); i++) {
                    String activityRestrictions = "INSERT into activityrestrictions(fk_activityid, fk_restrictionid) VALUES('"+maxActivityID+"',"
                            + " '"+activity.getRestrictionList().get(i).getID()+ "') ";
                    Statement stm = connection.createStatement();
                    stm.executeUpdate(activityRestrictions);
                }
            } catch (SQLException ex) {
                Logger.getLogger(ActivityConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            
        } catch (SQLException ex) {
            Logger.getLogger(ActivityConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
    public void editActivity(Activity activity) {
    
        String sql = "UPDATE activity SET"
                + "name = ?, instructor = ?,"
                + "(UPDATE days SET monday = ?, tuesday = ?, wedensday = ?, thursday, friday = ?,"
                + "saturday = ?, sunday = ? WHERE (SELECT daysID_FK FROM activity WHERE id = " + activity.getID() +""
                + ")), start_time = ?, end_time = ? WHERE id = " + activity.getID();
        
        connection = DBConnection.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            
            preparedStatement.setString(1, activity.getName());
            
            preparedStatement.setString(2, activity.getInstructor());
            ;
            
            
            preparedStatement.setInt(3, activity.getDays()[0] );
            preparedStatement.setInt(4, activity.getDays()[1] );
            preparedStatement.setInt(5, activity.getDays()[2] );
            preparedStatement.setInt(6, activity.getDays()[3] );
            preparedStatement.setInt(7, activity.getDays()[4] );
            preparedStatement.setInt(8, activity.getDays()[5] );
            preparedStatement.setInt(9, activity.getDays()[6] );
            preparedStatement.setString(10, activity.getStartTime());
            preparedStatement.setString(11, activity.getEndTime());
            
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ActivityConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void deleteActivity(int id) {
    
        String sql = "DELETE FROM activity WHERE id = ? ";
        
        connection = DBConnection.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            
            preparedStatement.setInt(1, id);
            
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ActivityConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public ArrayList<Activity> getActivityList() {
    
        connection = DBConnection.getConnection();
        @SuppressWarnings({ "rawtypes", "unchecked" })
		ArrayList<Activity> activityList = new ArrayList();
        @SuppressWarnings({ "rawtypes", "unchecked" })
        ArrayList<int[]> arrayList = new ArrayList();
        String sql = "SELECT monday, tuesday, wedensday, thursday, friday, saturday, sunday FROM days";
        
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while(resultSet.next()) {
            
                int[] daysArray = new int[7];
                daysArray[0] = resultSet.getInt("monday");
                daysArray[1] = resultSet.getInt("tuesday");
                daysArray[2] = resultSet.getInt("wedensday");
                daysArray[3] = resultSet.getInt("thursday");
                daysArray[4] = resultSet.getInt("friday");
                daysArray[5] = resultSet.getInt("saturday");
                daysArray[6] = resultSet.getInt("sunday");
                
                arrayList.add(daysArray);
                
            }
            
            String activities = "SELECT name, max_participants, instructor,"
                    + "daysID_FK, start_time, end_time FROM activity";
            
            Statement statement2 = connection.createStatement();
            ResultSet result = statement2.executeQuery(activities);
            int i = 0;
            while(result.next()) {
            
                activityList.add(new Activity(result.getByte("id"), result.getString("name")
                        , result.getString("instructor"), result.getString("start_time"),
                        result.getString("end_time"), arrayList.get(i), null));
            }
              
            ResultSet s;
            for(int j = 0; j < activityList.size(); j++) {
                
                String restQuery = "SELECT * from restrictions WHERE ID = (SELECT fk_restrictionID from activityrestrictions WHERE fk_activityID = "+activityList.get(j)+")";
                Statement stm3 = connection.createStatement();
                s = stm3.executeQuery(restQuery);
                ArrayList<Restriction> restList = new ArrayList<>(); 
                while(s.next()) {
                int id = s.getInt("restrictionID");
                String restName = s.getString("restrictionName");
                int restvalue = s.getInt("restrictionValue");
                
                Restriction r = new Restriction(id, restName, restvalue); 
                restList.add(r);
                }
                activityList.get(j).setList(restList);
            }
            
            
            
            
        } catch (SQLException ex) {
            Logger.getLogger(ActivityConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return activityList;
    }

    private int getdays(Connection connection) {
        
        String sql = "SELECT MAX(ID) FROM days";
        try {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);
            
            if(result.next()) {
            
                return result.getInt("MAX(ID)");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ActivityConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
        private int getMaxActivityID(Connection connection) {
        
        String sql = "SELECT MAX(activityID) FROM Activity";
        try {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);
            
            if(result.next()) {
            
                return result.getInt("MAX(activityID)")+1;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ActivityConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
      
    
}
