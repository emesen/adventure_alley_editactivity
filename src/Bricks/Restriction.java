/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bricks;

/**
 *
 * @author Mal
 */
public class Restriction {
    
    private int ID;
    private String name;
    private int restrictionValue;
    
    public Restriction(int ID, String name, int restrictionValue) {
        this.ID = ID;
        this.name = name;
        this.restrictionValue = restrictionValue;
        
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getRestrictionValue() {
        return restrictionValue;
    }

    public void setRestrictionValue(int restrictionValue) {
        this.restrictionValue = restrictionValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRestriction() {
        return restrictionValue;
    }

    public void setRestriction(int restrictionValue) {
        this.restrictionValue = restrictionValue;
    }
    
}
